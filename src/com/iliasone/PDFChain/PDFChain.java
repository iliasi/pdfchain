/**
 * Simple PDF file merger and converter based on Apache PDFBox library
 * Created by Ilias Ibrahim 
 */

package com.iliasone.PDFChain;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class PDFChain extends JFrame {
	
	
	private JButton btn1Panel1;
	private JButton btn2Panel1;
	private JButton btn3Panel1;
	private JButton mergeBtn;
	private JLabel lbl1Panel1;
	private JLabel lbl2Panel1;
	private JLabel lbl3Panel1;
	
	
	private JButton btn1Panel2;
	private JButton btn3Panel2;
	private JLabel lbl1Panel2;
	
	
	
	private File pdfFilePath;
	private File imgFilePath;
	
	
	public void filePathPdf() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PDF Documents", "pdf"));
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			//System.out.println("Selected file: " + selectedFile.getAbsolutePath());
			pdfFilePath = selectedFile.getAbsoluteFile();
		}else if (result == JFileChooser.CANCEL_OPTION){
			pdfFilePath = null;
		}
	}
	
	public void filePathImg() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "png", "gif", "bmp"));
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			//System.out.println("Selected file: " + selectedFile.getAbsolutePath());
			imgFilePath = selectedFile.getAbsoluteFile();
		}else if (result == JFileChooser.CANCEL_OPTION){
			imgFilePath = null;
		}
	}
	
	
	
	
	
	
	public PDFChain() {
		setTitle("PDF Tool");
		setResizable(false);
		getContentPane().setLayout(null);
		
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		
		//Buttons for First Panel (Merge PDF)
		btn1Panel1 = new JButton("Select File 1");
		btn1Panel1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				filePathPdf();
				lbl1Panel1.setText(pdfFilePath.toString());
			}
		});
		btn1Panel1.setBounds(26, 50, 123, 23);
		btn2Panel1 = new JButton("Select File 2");
		btn2Panel1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				filePathPdf();
				lbl2Panel1.setText(pdfFilePath.toString());
			}
		});
		btn2Panel1.setBounds(26, 84, 123, 23);
		btn3Panel1 = new JButton("Select File 3");
		btn3Panel1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				filePathPdf();
				lbl3Panel1.setText(pdfFilePath.toString());
			}
		});
		btn3Panel1.setBounds(26, 119, 123, 23);
		mergeBtn = new JButton("Merge Files");
		mergeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Need to fix permission issues --
				//filePathDestination();
				PDFMergerUtility pdfMerger = new PDFMergerUtility();
				pdfMerger.setDestinationFileName(System.getProperty("user.home")+ "//Desktop//merger.pdf");
				try {
					pdfMerger.addSource(lbl1Panel1.getText());
					pdfMerger.addSource(lbl2Panel1.getText());
					//pdfMerger.addSource(lbl3Panel1.getText());
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        try {
					pdfMerger.mergeDocuments(null);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		mergeBtn.setBounds(215, 238, 123, 23);
		
		// Label To display file path (Panel 1)
		lbl1Panel1 = new JLabel("");
		lbl1Panel1.setBounds(159, 54, 386, 14);
		lbl2Panel1 = new JLabel("");
		lbl2Panel1.setBounds(159, 88, 386, 14);
		lbl3Panel1 = new JLabel("");
		lbl3Panel1.setBounds(159, 123, 386, 14);
		panel1.setLayout(null);
		
		//add components to Panel 1
		panel1.add(btn1Panel1);
		panel1.add(lbl1Panel1);
		panel1.add(btn2Panel1);
		panel1.add(lbl2Panel1);
		panel1.add(btn3Panel1);
		panel1.add(lbl3Panel1);
		panel1.add(mergeBtn);
		
		// Buttons for Second Panel (Convert Image to PDF)
		btn1Panel2 = new JButton("Select Image File");
		btn1Panel2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				filePathImg();
				lbl1Panel2.setText(imgFilePath.toString());
			}
		});
		btn1Panel2.setBounds(30, 71, 139, 23);
		btn3Panel2 = new JButton("Convert File");
		btn3Panel2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				try {
					//Creating a new pdf document to hold the image file
					PDDocument document = new PDDocument();
					
					InputStream in = new FileInputStream(lbl1Panel2.getText());
			        BufferedImage img = ImageIO.read(in);
			        
			        float width = img.getWidth();
			        float height = img.getHeight();
			        
			        PDPage newPage = new PDPage(new PDRectangle(width, height));
					document.addPage(newPage); // this adds the page to the document.
					
					PDImageXObject pdImage = PDImageXObject.createFromFile(lbl1Panel2.getText(),document);
					PDPageContentStream contents = new PDPageContentStream(document, newPage);
					contents.drawImage(pdImage, 0, 0);
					contents.close();
					in.close();
					document.save(System.getProperty("user.home")+ "//Desktop//imgtopdf.pdf");
					document.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btn3Panel2.setBounds(229, 238, 121, 23);
		
		// Labels To display file path (Panel 2)
		lbl1Panel2 = new JLabel("");
		lbl1Panel2.setBounds(179, 75, 366, 14);
		panel2.setLayout(null);
		
		// add components to Panel 2
		panel2.add(btn1Panel2);
		panel2.add(lbl1Panel2);
		panel2.add(btn3Panel2);
		
		
		
		JTabbedPane tab = new JTabbedPane();
		tab.setBounds(20, 20, 560, 300);
		tab.add("Merge PDF", panel1);
		tab.add("Convert Image to PDF", panel2);
		//tab.add("Save PDF as Image", panel3);
		
		getContentPane().add(tab);
	}

	
}
